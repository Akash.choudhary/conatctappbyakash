from operator import or_
from sqlite3 import IntegrityError
from sqlalchemy import func
from app import database as db,contactApp
from flask import Flask, redirect, render_template, request

class Contact(db.Model):
    __tablename__ ='contact'
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    number = db.Column(db.BigInteger, unique=True, nullable=False)
    isDeleted = db.Column(db.Boolean, default=False, nullable=False)
    def __repr__(self):
        return self

@contactApp.route('/' , methods=['GET','POST'])
def root():
    try:
        if request.method=="POST":
            name=request.form['name']
            email= request.form['email']
            phone= request.form['phone']
            contactData=Contact(name=name, number=phone, email=email)
            db.session.add(contactData)
            db.session.commit()
        contact=Contact.query.filter_by(isDeleted=False).all()
        # contact=Contact.query.all()
        return render_template('contact/index.html', contact=contact)
    except Exception as e:
        return render_template('contact/error.html', exception=e.args)


@contactApp.route('/delete/<int:sn>')
def delete(sn):
    deleteItem=Contact.query.filter_by(id=sn).first()
    # deleteItem.isDeleted=True
    # db.session.add(deleteItem)
    db.session.delete(deleteItem)
    db.session.commit()
    return redirect('/')

@contactApp.route('/update/<int:sn>', methods=['GET','POST'])
def update(sn):
    try:
        updateItem=Contact.query.filter_by(id=sn).first()
        if request.method=="POST":
            name=request.form['name']
            email= request.form['email']
            phone= request.form['phone']
            updateItem.name=name
            updateItem.email=email
            updateItem.number=phone
            db.session.add(updateItem)
            db.session.commit()
            return redirect('/')    
        return render_template('contact/update.html', contact=updateItem)
    except Exception as e:
        return render_template('contact/error.html', exception=e.args)

@contactApp.route('/search' , methods=['GET','POST'])
def search():
    if request.method=="POST":
        search=request.form['search']
        searchItem=Contact.query.filter(or_(func.lower(Contact.name)==func.lower(search), func.lower(Contact.email)==func.lower(search))).all()
    return render_template('contact/index.html', contact=searchItem, isSearch=True)
   
