from flask import Flask, render_template,Blueprint
from flask_sqlalchemy import SQLAlchemy


contactApp = Flask(__name__,instance_relative_config=True)   
try:
    contactApp.config.from_pyfile('config.py')
    # Creating an SQLAlchemy instance
    database = SQLAlchemy(contactApp)
    database.init_app(contactApp)
except Exception as e:
    render_template('contact/error.html', exception=e.args)

from app import models
